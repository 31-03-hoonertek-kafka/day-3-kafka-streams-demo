package com.classpath.kstreamsprocessing.service;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Properties;

import static org.apache.kafka.streams.kstream.Produced.with;

@Component
public class KafkaStreamsCapitalizeApp {


    //@Override
    public void run(String... args) throws Exception {
        MockDataProducer.produceRandomText();

        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "app_id");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "139.59.64.198:9092, 157.245.98.120:9092");

        //serializer and deserializer
        Serde<String> stringSerde = Serdes.String();

        //build the topology
        StreamsBuilder streamsBuilder = new StreamsBuilder();

        streamsBuilder
                .stream("src-topic", Consumed.with(stringSerde, stringSerde))
                .mapValues((ValueMapper<String, String>) String::toUpperCase)
                .to("dest-topic", with(stringSerde, stringSerde));

        //debugging purpose
        //sourceStream.print(Printed.<String,String>toSysOut().withLabel("capitalization-app"));
        //start the execution
        KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), properties);

        //start the kafka streams
        kafkaStreams.start();

        Thread.sleep(20_000);
        kafkaStreams.close();
    }
}
