package com.classpath.kstreamsprocessing.service;

import com.github.javafaker.Faker;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.IntegerSerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class MockDataProducer {
    public static void produceRandomText() throws InterruptedException {
        Faker faker = new Faker();
        Properties properties = properties();
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties);

       for(int i = 0; i < 100; i ++){
            ProducerRecord<String, String> producerRecord = new ProducerRecord("src-topic", i+"", faker.chuckNorris().fact());
            kafkaProducer.send(producerRecord);
            //Thread.sleep(5000);
        }
    }
    public static void produceRandomNumbers() throws InterruptedException {
        Faker faker = new Faker();
        Properties properties = properties();
        properties.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
        KafkaProducer<String, Integer> kafkaProducer = new KafkaProducer<>(properties);

       for(int i = 0; i < 10_000; i ++){
            ProducerRecord<String, Integer> producerRecord = new ProducerRecord("age-topic", i, faker.number().numberBetween(10 , 80));
            kafkaProducer.send(producerRecord);
        }
    }


    public static Properties properties() {
        Properties properties = new Properties();
        properties.put("bootstrap.servers", "139.59.64.198:9092,157.245.98.120:9092");
        properties.put("acks", "1");
        properties.put("retries", "3");
        return  properties;
    }
}
