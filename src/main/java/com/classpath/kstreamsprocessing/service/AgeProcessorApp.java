package com.classpath.kstreamsprocessing.service;

import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Properties;
import static org.apache.kafka.streams.kstream.Produced.with;

@Component
public class AgeProcessorApp implements CommandLineRunner {
    @Override
    public void run(String... args) throws Exception {
        MockDataProducer.produceRandomNumbers();

        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "app_id");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "139.59.64.198:9092, 157.245.98.120:9092");

        //serializer and deserializer
        Serde<Integer> integerSerde = Serdes.Integer();

        //build the topology
        StreamsBuilder streamsBuilder = new StreamsBuilder();

        streamsBuilder
                .stream("age-topic", Consumed.with(integerSerde, integerSerde))
                .filter((key, value) -> value < 18)
                .to("minor-users-topic", with(integerSerde, integerSerde));
        streamsBuilder
                .stream("age-topic", Consumed.with(integerSerde, integerSerde))
                .filter((key, value) -> value > 18 && value < 60)
                .to("major-users-topic", with(integerSerde, integerSerde));
        streamsBuilder
                .stream("age-topic", Consumed.with(integerSerde, integerSerde))
                .filter((key, value) -> value > 18 && value < 60)
                .to("senior-citizen-users-topic", with(integerSerde, integerSerde));

        KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), properties);

        kafkaStreams.start();

        System.out.println("Kafka-streams starter");
        Thread.sleep(20_000);
        kafkaStreams.close();
    }
}
