package com.classpath.kstreamsprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KstreamsProcessingApplication {

    public static void main(String[] args) {
        SpringApplication.run(KstreamsProcessingApplication.class, args);
    }

}
